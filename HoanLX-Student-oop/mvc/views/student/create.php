<style>
  .image {
    display: block;
    width: 100px;
    height: 100px;
    border-radius: 50%;
    object-fit: cover;
  }

  .error {
    background-color: #e36d6d;
    color: #fff;
    padding: 10px;
    margin-bottom: 10px;
  }
</style>
<h1>add student</h1>
<form action="<?php echo TFO_DOMAIN ?>/store" method="POST" name="frm1" enctype="multipart/form-data">
  <div class="mb-3">
    <label for="name" class="form-label">name</label>
    <input type="text" value="<?= isset($_POST['name']) ? $_POST['name'] : '' ?>" class="form-control" id="name" name="name">
  </div>
  <div class="mb-3">
    <label for="date" class="form-label">date of birth</label>
    <input type="date" value="<?= isset($_POST['date']) ? $_POST['date'] : '' ?>" class="form-control" id="date" name="date">
  </div>
  <div class="mb-3">
    <label for="address" class="form-label">address</label>
    <input type="text" value="<?= isset($_POST['address']) ? $_POST['address'] : '' ?>" class="form-control" id="address" name="address">
  </div>
  <div class="mb-3">
    <label for="avatar">Avatar</label>
    <input type="file" id="avatar" name="avatar[]" multiple>
  </div>
  <?php if (!empty($error)) {  ?>
    <div class="error"> <?php echo $error; ?></div>
  <?php } ?>

  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
</form>