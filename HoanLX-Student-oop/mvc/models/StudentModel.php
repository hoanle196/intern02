<?php
class StudentModel extends Hooks
{
  public function getRecode($params = null)
  {
    $conn = $this->connectDB();
    if ($params) {
      $sql = "SELECT
      s.*,
      i.image_path 
    FROM
      students s
      JOIN images i ON s.student_id = i.student_id
    WHERE s.student_id = :params";
      $stmt = $conn->prepare($sql);
      $stmt->execute(['params' => $params[0]]);
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      return $result;
    }
    $sql = "SELECT
    s.*,
    i.image_path 
    FROM
    students s
    JOIN images i ON s.student_id = i.student_id";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }
  public function addRecode($post, $files)
  {
    $error = '';
    if (isset($post['submit'])) {
      $name = isset($post['name']) ? $post['name'] : '';
      $date = isset($post['date']) ? $post['date'] : '';
      $address = isset($post['address']) ?  $post['address'] : '';
      $avatar = isset($files['avatar']) ? $files['avatar'] : [];
      $avatar_name = isset($files['avatar']['name']) ? $files['avatar']['name'] : [];
      $avatar_size = isset($files['avatar']['size']) ? $files['avatar']['size'] : [];
      $avatar_tmp_name = isset($files['avatar']['tmp_name']) ? $files['avatar']['tmp_name'] : [];
      $avatar_error = isset($files['avatar']['error']) ? $files['avatar']['error'] : [];
      // if (empty($name) || empty($date) || empty($address) || empty($avatar)) {
      //     $error .= "Field Can't be left blank <br>";
      // }
      // if (strlen($name) < 6 || strlen($name) > 100) {
      //     $error .= "must be between 6 and 100 characters <br>";
      // }
      // if ($avatar_error == 0) {
      //     $exten = strtolower(pathinfo($avatar_name, PATHINFO_EXTENSION));
      //     $arr = ['jpg', 'png', 'jpeg', 'gif'];
      //     if (!in_array($exten, $arr)) {
      //         $error .= "Upload file must be an image <br>";
      //     }
      //     $max = 2 * 1024 * 1024;
      //     if ($avatar_size > $max) {
      //         $error .= " file size is too big <br>";
      //     }
      // }
      if (empty($error)) {

        extract($post);
        $conn = $this->connectDB();
        $sql = "INSERT INTO students (student_name, student_birthday, student_address)
        VALUES ('$name', '$date','$address')";
        $stmt = $conn->prepare($sql);
        if ($stmt->execute()) {
          $res = $conn->lastInsertId();
          // static $res;
        }

        // function abc()
        // {
        //   global $res;
        // }
        array_map(function ($avatar_name, $avatar_tmp_name, $res) {
          $folder = "./public/uploads";
          if (!file_exists($folder)) {
            mkdir($folder);
          }
          $fileName = time() . "-" . strtolower(basename($avatar_name));
          $is_upload = move_uploaded_file($avatar_tmp_name, "$folder/$fileName");
          $conn = $this->connectDB();
          $sql = "INSERT INTO images (image_path, student_id) VALUES (?, ?)";
          $stmt = $conn->prepare($sql);
          $stmt->execute([$fileName, $res]);
        }, $avatar_name, $avatar_tmp_name, [$res]);
        return true;
      }
    }
  }
  public function update($post, $params)
  {
    extract($post);
    $conn = $this->connectDB();
    $sql =  "UPDATE students SET student_name = '$name', student_birthday = '$date', student_address = '$address'  WHERE student_id = :id";
    $stmt = $conn->prepare($sql);
    return $stmt->execute(['id' => $params[0]]);
  }
  public function destroy($params)
  {
    $conn = $this->connectDB();
    $sql =  "DELETE FROM students  WHERE student_id = :id";
    $stmt = $conn->prepare($sql);
    return $stmt->execute(['id' => $params[0]]);
  }
}
