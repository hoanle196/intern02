// $("form").validate({
//     rules: {
//         // simple rule, converted to {required:true}
//         username: "required",
//         // compound rule
//         password: {
//             required: true,
//             minlength: 5
//         }
//     }
// });
$('form').validate({
    rules: {
        username: 'required',
        password: {
            minlength: 5,
            required: true
        }
    }
});
// Can also be included with a regular script tag
// import Typed from 'typed.js';
// $('form').submit(function(e) {
//     $('label.error').remove();
//     isError = false;
//     const valUsername = $('[name=username]').val();
//     const valPass = $('[name=password]').val();
//     if (!valUsername) {
//         isError = true;
//         $('[name=username]').after(`<label class="error">vui long nhap username</label>`)
//     }
//     if (!valPass) {
//         isError = true;
//         $('[name=password]').after(`<label class="error">vui long nhap password</label>`)
//     }
//     if (isError) return false
//     return true;
// })
var typed = new Typed(".typed-words", {
    strings: [
        "Developer.",
        " tester.",
        " designer.",
        " leader.",
        " project manager.",
    ],
    typeSpeed: 80,
    backSpeed: 80,
    backDelay: 4000,
    startDelay: 1000,
    loop: true,
    showCursor: true,
});