<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Quản lý sinh viên</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../public/css/style.css">
</head>

<body>
    <div class="container" style="margin-top:20px;">
        <?php
        session_start();
        $message = '';
        $class = '';
        if (!empty($_SESSION['success'])) {
            $message = $_SESSION['success'];
            //xóa phần tử có key là success
            unset($_SESSION['success']);
            $class = 'success';
        } else if (!empty($_SESSION['error'])) {
            $message = $_SESSION['error'];
            //xóa phần tử có key là success
            unset($_SESSION['error']);
            $class = 'danger';
        }
        ?>
        <?php if ($message) : ?>
            <div class="alert alert-<?= $class ?> mt-3"><?= $message ?></div>
        <?php endif ?>