<?php require '../layout/header.php' ?>
<h1>Edit student</h1>
<?php
$error = '';
$success = '';
if (isset($_POST['submit'])) {
    $name = isset($_POST['name']) ? $_POST['name'] : '';
    $date = isset($_POST['date']) ? $_POST['date'] : '';
    $address = isset($_POST['address']) ?  $_POST['address'] : '';

    $avatar = isset($_FILES['avatar']) ? $_FILES['avatar'] : '';
    $avatar_name = isset($_FILES['avatar']['name']) ? $_FILES['avatar']['name'] : '';
    $avatar_size = isset($_FILES['avatar']['size']) ? $_FILES['avatar']['size'] : '';
    $avatar_tmp_name = isset($_FILES['avatar']['tmp_name']) ? $_FILES['avatar']['tmp_name'] : '';
    $avatar_error = isset($_FILES['avatar']['error']) ? $_FILES['avatar']['error'] : '';
    if (empty($name) || empty($date) || empty($address) || empty($avatar)) {
        $error .= "Field Can't be left blank <br>";
    }
    if (strlen($name) < 6 || strlen($name) > 100) {
        $error .= "must be between 6 and 100 characters <br>";
    }
    if ($avatar_error == 0) {
        $exten = strtolower(pathinfo($avatar_name, PATHINFO_EXTENSION));
        $arr = ['jpg', 'png', 'jpeg', 'gif'];
        if (!in_array($exten, $arr)) {
            $error .= "Upload file must be an image <br>";
        }
        $max = 2 * 1024 * 1024;
        if ($avatar_size > $max) {
            $error .= " file size is too big <br>";
        }
    }
    if (empty($error)) {
        $folder = "../public/uploads";
        if (!file_exists($folder)) {
            mkdir($folder);
        }
        $fileName = time() . "-" . strtolower(basename($avatar_name));
        $is_upload = move_uploaded_file($avatar_tmp_name, "$folder/$fileName");
        if ($is_upload) {
            $_SESSION['data'][$name]['name'] = $name;
            $_SESSION['data'][$name]['date'] = $date;
            $_SESSION['data'][$name]['address'] = $address;
            $_SESSION['data'][$name]['img'] = $fileName;
            $_SESSION['success'] = 'update success';
            // var_dump($_SESSION);
            // exit;
            header('location:index.php');
            exit;
        } else {
            $error .= " you need to reload the photo ";
        }
    }
}
?>
<style>
    .image {
        display: block;
        width: 100px;
        height: 100px;
        border-radius: 50%;
        object-fit: cover;
    }

    .error {
        background-color: #e36d6d;
        color: #fff;
        padding: 10px;
        margin-bottom: 10px;
        border-radius: 4px;
    }
</style>
<?php
$info = $_SESSION['data'][$_GET['name']];
// var_dump($name);
// var_dump($arr);
// exit;
?>
<form action="" method="POST" name="frm1" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">name</label>
        <input type="text" value="<?= isset($info) ? $info['name'] : '' ?>" class="form-control" id="name" name="name">
    </div>
    <div class="mb-3">
        <label for="date" class="form-label">date of birth</label>
        <input type="date" value="<?= isset($info) ? $info['date'] : '' ?>" class="form-control" id="date" name="date">
    </div>
    <div class="mb-3">
        <label for="address" class="form-label">address</label>
        <input type="text" value="<?= isset($info) ? $info['address'] : '' ?>" class="form-control" id="address" name="address">
    </div>
    <div class="mb-3">
        <label for="avatar">Avatar</label>
        <input type="file" id="avatar" name="avatar">
    </div>
    <img src="../public/uploads/<?= isset($info) ? $info['img'] : ''  ?>" class="image" alt="">
    <?php if (!empty($error)) {  ?>
        <div class="error"> <?php echo $error; ?></div>
    <?php } ?>

    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
</form>
<?php require '../layout/footer.php' ?>
<script>
    $("[name=avatar]").change(function(event) {
        $('img.image').remove();
        const {
            files
        } = event.target;
        debugger;
        const url = window.URL.createObjectURL(files[0]);
        $('[name=avatar]').after(`<img src="${url}" class = "image" alt="">`);
        // var a= event.target;
        // var b = a.files;
        // console.log(b);
    })
</script>