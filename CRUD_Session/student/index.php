<?php require '../layout/header.php' ?>
<h1>List student</h1>
<a href="create.php" class="btn btn-info">Add</a>
<table class="table table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>name</th>
      <th>birthday</th>
      <th>address</th>
      <th>action</th>
    </tr>
  </thead>
  <tbody>
    <?php
    // unset($_SESSION['data']);
    // exit;
    $order = 0;
    if (!empty($_SESSION['data'])) :
      foreach ($_SESSION['data'] as $name => $info) :
        $order++;
    ?>
        <tr>
          <td><?= $order ?></td>
          <td><img style="
            width: 50px;
            height: 50px;
            border-radius: 50%;
            object-fit: cover;" class="avatar" src="../public/uploads/<?= $info['img'] ?>" alt="avatar"><?= $info['name'] ?></td>
          <td><?= $info['date'] ?></td>
          <td><?= $info['address'] ?></td>
          <td>
            <a class="btn btn-warning btn-sm" href="edit.php?name=<?= $name ?>">Edit</a>
            <button type="button" data-href="destroy.php?name=<?= $name ?>" class="btn btn-danger btn-sm delete" data-toggle="modal" data-target="#exampleModal">
              Delete
            </button>
          </td>
        </tr>
    <?php
      endforeach;
    endif;
    ?>
  </tbody>
</table>
<div>
  <span>Số lượng: <?= $order ?></span>
</div>
<?php require '../layout/footer.php' ?>