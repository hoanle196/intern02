<?php
session_start();
$name = $_GET['name'];
if (isset($_SESSION['data'][$name])) {
    unset($_SESSION['data'][$name]);
    $_SESSION['success'] = 'delete success';
    header('location:index.php');
    exit;
}
$_SESSION['error'] = 'delete fail';
header('location:index.php');
exit;
