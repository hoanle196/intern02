<?php
class RegisterController
{
          function index()
          {
                    $search = $_GET['search'] ?? "";
                    $registerRepository = new RegisterRepository;
                    if (!$search) {
                              $registers = $registerRepository->getAll(); // $registers dang chua danh sach (array) cac doi tuong sinh vien

                    } else {
                              $registers = $registerRepository->getbysearch($search); // $registers dang chua danh sach (array) cac doi tuong sinh vien

                    }
                    require_once('views/register/index.php');
          }
          function create()
          {
                    $studentRepository = new StudentRepository;
                    $students = $studentRepository->getAll();
                    $subjectRepository = new SubjectRepository;
                    $subjects = $subjectRepository->getAll();
                    require_once('views/register/create.php');
          }
          function store()
          {
                    $registerRepository = new RegisterRepository;
                    if ($registerRepository->save($_POST)) {
                              $_SESSION['success'] = 'đã đăng ký môn học thành công !';
                              header('location:/?c=register');
                              exit;
                    } else {
                              $_SESSION['error'] = $registerRepository->error;
                              header('location:/?c=register');
                              exit;
                    }
          }
          function update()
          {
                    // var_dump($_POST);
                    $registerRepository = new RegisterRepository;
                    $registerup = $registerRepository->update($_POST, $_GET);
                    if ($registerup) {
                              $_SESSION['success'] = 'đã update điểm thành công !';
                              header('location:/?c=register');
                              exit;
                    } else {
                              $_SESSION['error'] = $registerRepository->error;
                              header('location:/?c=register');
                              exit;
                    }
          }
          function delete()
          {
                    $registerRepository = new RegisterRepository;
                    $registerdel = $registerRepository->delete($_GET);
                    if ($registerdel) {
                              $_SESSION['success'] = 'đã xoá đăng ký môn học thành công !';
                              header('location:/?c=register');
                              exit;
                    } else {
                              $_SESSION['error'] = $registerRepository->error;
                              header('location:/?c=register');
                              exit;
                    }
          }
          function edit()
          {
                    $id = $_GET['id'];
                    $registerRepository = new RegisterRepository;
                    $row = $registerRepository->getone($id);
                    require_once('views/register/edit.php');
          }
}
