<?php
class StudentController
{
  function index()
  {
    $search = $_GET['search'] ?? "";
    $studentRepository = new StudentRepository;
    if (!$search) {
      $students = $studentRepository->getAll(); // $students dang chua danh sach (array) cac doi tuong sinh vien

    } else {
      $students = $studentRepository->getbysearch($search); // $students dang chua danh sach (array) cac doi tuong sinh vien

    }
    require_once('views/student/index.php');
  }
  function create()
  {
    require_once('views/student/create.php');
  }
  function store()
  {
    $studentRepository = new StudentRepository;
    if ($studentRepository->save($_POST)) {
      $_SESSION['success'] = 'đã tạo sinh viên thành công !';
      header('location:?c=student');
      exit;
    } else {
      $_SESSION['error'] = $studentRepository->error;
      header('location:/');
      exit;
    }
  }
  function update()
  {
    // var_dump($_POST);
    $studentRepository = new StudentRepository;
    $studentup = $studentRepository->update($_POST, $_GET);
    if ($studentup) {
      $_SESSION['success'] = 'đã update viên thành công !';
      header('location:/');
      exit;
    } else {
      $_SESSION['error'] = $studentRepository->error;
      header('location:/');
      exit;
    }
  }
  function delete()
  {
    $studentRepository = new StudentRepository;
    $studentdel = $studentRepository->delete($_GET);
    $registerRepositoty = new RegisterRepository;
    $registers = $registerRepositoty->getBystudentId($_GET);
    if (count($registers) > 0) {
      $_SESSION['error'] = 'Sinh viên này đã đăng ký môn học không thể xoá !';
      header('location:/backend/qlsvmvc/');
      exit;
    }
    if ($studentdel) {
      $_SESSION['success'] = 'đã xoá sinh viên thành công !';
      header('location:/backend/qlsvmvc/');
      exit;
    } else {
      $_SESSION['error'] = $studentRepository->error;
      header('location:/');
      exit;
    }
  }
  function edit()
  {
    $id = $_GET['id'];
    $studentRepository = new StudentRepository;
    $row = $studentRepository->getone($id);
    require_once('views/student/edit.php');
  }
}
