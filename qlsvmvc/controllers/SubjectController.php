<?php
class SubjectController
{
  function index()
  {
    $search = $_GET['search'] ?? "";
    $subjectRepository = new SubjectRepository;
    if (!$search) {
      $subjects = $subjectRepository->getAll(); // $subjects dang chua danh sach (array) cac doi tuong sinh vien

    } else {
      $subjects = $subjectRepository->getbysearch($search); // $subjects dang chua danh sach (array) cac doi tuong sinh vien

    }
    require_once('views/subject/index.php');
  }
  function create()
  {
    require_once('views/subject/create.php');
  }
  function store()
  {
    $subjectRepository = new SubjectRepository;
    if ($subjectRepository->save($_POST)) {
      $_SESSION['success'] = 'đã tạo môn học thành công !';
      header('location:/?c=subject');
      exit;
    } else {
      $_SESSION['error'] = $subjectRepository->error;
      header('location:/?c=subject');
      exit;
    }
  }
  function update()
  {
    // var_dump($_POST);
    $subjectRepository = new SubjectRepository;
    $subjectup = $subjectRepository->update($_POST, $_GET);
    if ($subjectup) {
      $_SESSION['success'] = 'đã update môn học thành công !';
      header('location:/?c=subject');
      exit;
    } else {
      $_SESSION['error'] = $subjectRepository->error;
      header('location:/?c=subject');
      exit;
    }
  }
  function delete()
  {
    $subjectRepository = new SubjectRepository;
    $subjectdel = $subjectRepository->delete($_GET);
    $registerRepository = new RegisterRepository;
    $register = $registerRepository->getBySubjectId($_GET);
    if (count($register) > 0) {
      $_SESSION['error'] = "Môn học này đã có sinh viên đăng ký ! không thể xoá ";
      header('location:/?c=subject');
      exit;
    }

    if ($subjectdel) {
      $_SESSION['success'] = 'đã xoá môn học thành công !';
      header('location:/?c=subject');
      exit;
    } else {
      $_SESSION['error'] = $subjectRepository->error;
      header('location:/?c=subject');
      exit;
    }
  }
  function edit()
  {
    $id = $_GET['id'];
    $subjectRepository = new SubjectRepository;
    $row = $subjectRepository->getone($id);
    require_once('views/subject/edit.php');
  }
}
