<?php require_once 'layout/header.php'; ?>
<h1>Danh sách sinh viên</h1>
<a href="?c=student&a=create" class="btn btn-info">Add</a>
<?php require_once 'layout/search.php'; ?>
<table class="table table-hover">
          <thead>
                    <tr>
                              <th>#</th>
                              <th>Mã SV</th>
                              <th>Tên</th>
                              <th>Ngày Sinh</th>
                              <th>Giới Tính</th>
                              <th>Thao Tác</th>
                    </tr>
          </thead>
          <tbody>
                    <?php $dem = 0;
                    foreach ($students as $student) {
                              $dem++; ?>
                              <tr>
                                        <td><?= $dem ?></td>
                                        <td><?= $student->id ?></td>
                                        <td><?= $student->name ?></td>
                                        <td><?= $student->birthday ?></td>
                                        <td><?= $student->gender ?></td>
                                        <td>
                                                  <a class="btn btn-warning btn-sm" href="?a=edit&id=<?= $student->id ?>">Sửa</a>
                                                  <button type="button" data-href="?a=delete&id=<?= $student->id ?>" class="btn btn-danger btn-sm delete aaa" data-toggle="modal" data-target="#exampleModal">
                                                            Xóa
                                                  </button>
                                        </td>
                              </tr>
                    <?php } ?>

          </tbody>
</table>
<div>
          <span>Số lượng:<?= count($students) ?></span>
</div>
<?php require_once 'layout/footer.php'; ?>