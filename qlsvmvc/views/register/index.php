<?php require_once 'layout/header.php'; ?>
<h1>Đăng ký môn học</h1>
<a href="?c=register&a=create" class="btn btn-info">Add</a>
<?php require_once 'layout/search.php'; ?>
<table class="table table-hover">
          <thead>
                    <tr>
                              <th>#</th>
                              <th>Mã SV</th>
                              <th>Tên SV</th>
                              <th>Mã MH</th>
                              <th>Tên MH</th>
                              <th>Điểm</th>
                              <th>Action</th>
                    </tr>
          </thead>
          <tbody>
                    <?php $dem = 0;
                    foreach ($registers as $register) {
                              $dem++ ?>
                              <tr>
                                        <td><?= $dem  ?></td>
                                        <td><?= $register->student_id ?></td>
                                        <td><?= $register->student_name  ?></td>
                                        <td><?= $register->subject_id ?></td>
                                        <td><?= $register->subject_name  ?></td>
                                        <td><?= $register->score ?></td>
                                        <td>
                                                  <a class="btn btn-warning btn-sm" href="?c=register&a=edit&id=<?= $register->id ?>">Sửa</a>
                                                  <button type="button" data-href="?c=register&a=delete&id=<?= $register->id ?>" class="btn btn-danger btn-sm delete aaa" data-toggle="modal" data-target="#exampleModal">
                                                            Xóa
                                                  </button>
                                        </td>
                              </tr>
                    <?php } ?>

          </tbody>
</table>
<div>
          <span>Số lượng:<?= count($registers) ?></span>
</div>
<?php require_once 'layout/footer.php'; ?>