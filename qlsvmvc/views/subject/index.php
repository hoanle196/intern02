<?php require_once 'layout/header.php'; ?>
<h1>Danh sách môn học</h1>
<a href="?c=subject&a=create" class="btn btn-info">Add</a>
<?php require_once 'layout/search.php'; ?>
<table class="table table-hover">
          <thead>
                    <tr>
                              <th>#</th>
                              <th>Mã MH</th>
                              <th>Tên</th>
                              <th>Số tín chỉ</th>
                              <th colspan="2">Tùy Chọn</th>
                    </tr>
          </thead>
          <tbody>
                    <?php $dem = 0;
                    foreach ($subjects as $subject) {
                              $dem++; ?>
                              <tr>
                                        <td><?= $dem ?></td>
                                        <td><?= $subject->id ?></td>
                                        <td><?= $subject->name ?></td>
                                        <td><?= $subject->number_of_credit ?></td>
                                        <td>
                                                  <a class="btn btn-warning btn-sm" href="?c=subject&a=edit&id=<?= $subject->id ?>">Sửa</a>
                                                  <button type="button" data-href="?c=subject&a=delete&id=<?= $subject->id ?>" class="btn btn-danger btn-sm delete aaa" data-toggle="modal" data-target="#exampleModal">
                                                            Xóa
                                                  </button>
                                        </td>
                              </tr>
                    <?php } ?>
          </tbody>
</table>
<div>
          <span>Số lượng:<?= count($subjects) ?></span>
</div>
<?php require_once 'layout/footer.php'; ?>