<?php
class StudentRepository
{
          public $error;
          //lay cac dong du lieu trong bang student va chuyen thanh danh sach doi tuong student
          protected function fetch($cond = null)
          {
                    global $conn;
                    $sql = "SELECT * FROM student";
                    if ($cond) {
                              $sql .= " WHERE $cond";
                              // $sql = "SELECT * FROM student WHERE $cond";
                    }
                    $result = $conn->query($sql);
                    $students = [];
                    if ($result->num_rows > 0) {
                              while ($row = $result->fetch_assoc()) {
                                        $id = $row['id'];
                                        $name = $row['name'];
                                        $birthday = $row['birthday'];
                                        $gender = $row['gender'];
                                        $student = new student($id, $name, $birthday, $gender);
                                        $students[] = $student; // [] them 1 phan tu vao cuoi ds mang
                              }
                    }
                    return $students;
          }
          public function getAll()
          {
                    return $this->fetch();
          }
          public function getbysearch($search)
          {
                    $cond = "name LIKE '%$search%'";
                    $student = $this->fetch($cond);
                    return $student;
          }
          public function save($data)
          {
                    global $conn;
                    $name = $data['name'];
                    $birthday = $data['birthday'];
                    $gender = $data['gender'];
                    $sql = "INSERT INTO student(name,birthday,gender)
                              VALUE ('$name','$birthday','$gender')";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = $sql . '<br>' . $conn->connect_error;
                    return false;
          }
          public function update($data, $get)
          {
                    global $conn;
                    $id = $get['id'];
                    $name = $data['name'];
                    $birthday = $data['birthday'];
                    $gender = $data['gender'];
                    $sql = "UPDATE student SET name = '$name',birthday = '$birthday',gender='$gender' WHERE id = $id";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = "loi truy van :" . "$sql" . "<br>" . $conn->connect_error;
                    return false;
          }
          public function delete($get)
          {
                    global $conn;
                    $id = $get['id'];
                    $sql = "DELETE FROM student WHERE id = $id";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = "loi truy van :" . "$sql" . "<br>" . $conn->connect_error;
                    return false;
          }
          public function getone($id)
          {         //cach1 return array
                    // global $conn;
                    // $sql = "SELECT * FROM student WHERE id = $id ";
                    // $result = $conn->query($sql);
                    // if ($result->num_rows > 0) {
                    //           $arr = $result->fetch_assoc();
                    //           return $arr;
                    // }
                    //cach2 return obj
                    $cond = "id = $id";
                    $students = $this->fetch($cond);
                    // lay ra 1 phan tu dau tien trong danh sach (student)
                    $student = current($students);
                    return $student;
          }
}
