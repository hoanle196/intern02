<?php
class SubjectRepository
{
          public $error;
          //lay cac dong du lieu trong bang subject va chuyen thanh danh sach doi tuong subject
          protected function fetch($cond = null)
          {
                    global $conn;
                    $sql = "SELECT * FROM subject";
                    if ($cond) {
                              $sql .= " WHERE $cond";
                              // $sql = "SELECT * FROM subject WHERE $cond";
                    }
                    $result = $conn->query($sql);
                    $subjects = [];
                    if ($result->num_rows > 0) {
                              while ($row = $result->fetch_assoc()) {
                                        $id = $row['id'];
                                        $name = $row['name'];
                                        $number_of_credit = $row['number_of_credit'];
                                        $subject = new subject($id, $name, $number_of_credit);
                                        $subjects[] = $subject; // [] them 1 phan tu vao cuoi ds mang
                              }
                    }
                    return $subjects;
          }
          public function getAll()
          {
                    return $this->fetch();
          }
          public function getbysearch($search)
          {
                    $cond = "name LIKE '%$search%'";
                    $subject = $this->fetch($cond);
                    return $subject;
          }
          public function save($data)
          {
                    global $conn;
                    $name = $data['name'];
                    $number_of_credit = $data['number_of_credit'];
                    $sql = "INSERT INTO subject(name,number_of_credit)
                              VALUE ('$name','$number_of_credit')";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = $sql . '<br>' . $conn->connect_error;
                    return false;
          }
          public function update($data, $get)
          {
                    global $conn;
                    $id = $get['id'];
                    $name = $data['name'];
                    $number_of_credit = $data['number_of_credit'];
                    $sql = "UPDATE subject SET name = '$name',number_of_credit = '$number_of_credit' WHERE id = $id";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = "loi truy van :" . "$sql" . "<br>" . $conn->connect_error;
                    return false;
          }
          public function delete($get)
          {
                    global $conn;
                    $id = $get['id'];
                    $sql = "DELETE FROM subject WHERE id = $id";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = "loi truy van :" . "$sql" . "<br>" . $conn->connect_error;
                    return false;
          }
          public function getone($id)
          {         //cach1 return array
                    // global $conn;
                    // $sql = "SELECT * FROM subject WHERE id = $id ";
                    // $result = $conn->query($sql);
                    // if ($result->num_rows > 0) {
                    //           $arr = $result->fetch_assoc();
                    //           return $arr;
                    // }
                    //cach2 return obj
                    $cond = "id = $id";
                    $subjects = $this->fetch($cond);
                    // lay ra 1 phan tu dau tien trong danh sach (subject)
                    $subject = current($subjects);
                    return $subject;
          }
}
