<?php
class RegisterRepository
{
          public $error;
          //lay cac dong du lieu trong bang register va chuyen thanh danh sach doi tuong register
          protected function fetch($cond = null)
          {
                    global $conn;
                    $sql = "SELECT register.*, student.name AS student_name, subject.name AS subject_name FROM register
                    JOIN student ON student.id=register.student_id
                    JOIN subject ON subject.id=register.subject_id";
                    if ($cond) {
                              $sql .= " WHERE $cond";
                              // $sql = "SELECT * FROM register WHERE $cond";
                    }
                    $result = $conn->query($sql);
                    $registers = [];
                    if ($result->num_rows > 0) {
                              while ($row = $result->fetch_assoc()) {
                                        $id = $row['id'];
                                        $student_id = $row['student_id'];
                                        $subject_id = $row['subject_id'];
                                        $score = $row['score'];
                                        $student_name = $row['student_name'];
                                        $subject_name = $row['subject_name'];
                                        $register = new register($id, $student_id, $subject_id, $score, $student_name, $subject_name);
                                        $registers[] = $register; // [] them 1 phan tu vao cuoi ds mang
                              }
                    }
                    return $registers;
          }
          public function getAll()
          {
                    return $this->fetch();
          }
          public function getbysearch($search)
          {
                    $cond = "student.name LIKE '%$search%' OR subject.name LIKE '%$search%' ";
                    $register = $this->fetch($cond);
                    return $register;
          }
          public function save($data)
          {
                    global $conn;
                    $student_id = $data['student_id'];
                    $subject_id = $data['subject_id'];
                    $sql = "INSERT INTO register(student_id,subject_id)
                              VALUE ('$student_id','$subject_id')";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = $sql . '<br>' . $conn->connect_error;
                    return false;
          }
          public function update($data, $get)
          {
                    global $conn;
                    $id = $get['id'];
                    $score = $data['score'];
                    $sql = "UPDATE register SET score='$score' WHERE id = $id";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = "loi truy van :" . "$sql" . "<br>" . $conn->connect_error;
                    return false;
          }
          public function delete($get)
          {
                    global $conn;
                    $id = $get['id'];
                    $sql = "DELETE FROM register WHERE id = $id";
                    if ($conn->query($sql)) {
                              return true;
                    }
                    $this->error = "loi truy van :" . "$sql" . "<br>" . $conn->connect_error;
                    return false;
          }
          public function getone($id)
          {         //cach1 return array
                    // global $conn;
                    // $sql = "SELECT * FROM register WHERE id = $id ";
                    // $result = $conn->query($sql);
                    // if ($result->num_rows > 0) {
                    //           $arr = $result->fetch_assoc();
                    //           return $arr;
                    // }
                    //cach2 return obj
                    $cond = "register.id = $id";
                    $registers = $this->fetch($cond);
                    // lay ra 1 phan tu dau tien trong danh sach (register)
                    $register = current($registers);
                    return $register;
          }
          function getBystudentId($data) // tra ve ds dang ky mon hoc cua ban sinh vien cu the
          {
                    $id = $data['id'];
                    $cond = "student_id = $id";
                    $registers = $this->fetch($cond);
                    return $registers;
          }
          function getBySubjectId($data)
          { // tra ve ds mon hoc dc sinh vien dang ky 
                    $id = $data['id'];
                    $cond = "subject_id = $id";
                    $registers = $this->fetch($cond);
                    return $registers;
          }
}
