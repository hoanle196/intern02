<?php
session_start();
//router
//config
require_once('config/config.php');
require_once('config/connectDb.php');
$classctl = $_GET['c'] ?? 'student';
$function = $_GET['a'] ?? 'index';
$controller = ucfirst($classctl) . 'Controller'; // StudentController
//import require controller & model student
require_once "controllers/$controller.php";
require_once('models/student.php');
require_once('models/studentRepository.php');
//import require model subject
require_once('models/subject.php');
require_once('models/subjectRepository.php');
//import require model subject
require_once('models/register.php');
require_once('models/registerRepository.php');
$objcontroller = new $controller();
$objcontroller->$function();
