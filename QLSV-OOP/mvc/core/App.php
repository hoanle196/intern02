<?php
class App
{
    public function __construct($c = null)
    {
        $classctl = $_GET['c'] ?? 'student';
        $function = $_GET['a'] ?? 'index';
        if ($c) {
            $classctl = $c;
        }
        $controller = ucfirst($classctl) . 'Controller'; // StudentController
        require_once("./mvc/controllers/$controller.php");
        $objcontroller = new $controller();
        $objcontroller->$function();
    }
}
