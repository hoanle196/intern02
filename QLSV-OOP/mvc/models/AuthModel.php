<?php
class AuthModel extends Hooks
{
  public function register($data)
  {
    $conn = $this->connectDB();
    extract($data);
    $new_pass = password_hash($password, PASSWORD_BCRYPT);
    $sql = "INSERT INTO users (user_name, user_password, user_email, user_phone, user_role)
            VALUES (?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    return $stmt->execute([$name, $new_pass, $email, $phone_number, $roles]);
  }
  public function checkUser($data)
  {
    extract($data);
    $conn = $this->connectDB();
    $sql = "SELECT * FROM users WHERE user_email = ? ";
    $stmt = $conn->prepare($sql);
    if ($stmt->execute([$email])) {
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      $count = $stmt->rowCount();
      echo json_encode(['count' => $count]);
      return $result;
    }
    return false;
  }
  public function updateUser($data, $id)
  {
    extract($data);
    $new_pass = password_hash($password, PASSWORD_BCRYPT);
    $conn = $this->connectDB();
    $sql = "UPDATE users SET user_password = ? WHERE user_id = ?";
    $stmt = $conn->prepare($sql);
    return $stmt->execute([$new_pass, $id]);
  }
}
