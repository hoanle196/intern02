<?php
class Admin extends User
{
  public function canManageOrders()
  {
    return true;
  }

  public function canManageCustomers()
  {
    return true;
  }
}
