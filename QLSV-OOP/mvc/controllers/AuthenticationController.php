<?php
class AuthenticationController extends Hooks
{
  public function index()
  {
    unset($_SESSION['login']);
    $this->title = 'login';
    $this->content = $this->view('Auth.login');
    $this->layout('admin');
  }
  public function createAdmin()
  {
    $this->title = 'register Admin';
    $this->content = $this->view('Auth.register-admin');
    $this->layout('admin');
  }
  public function createCustomer()
  {
    $this->title = 'register Customer';
    $this->content = $this->view('Auth.register-customer');
    $this->layout('admin');
  }
  public function forgotPassword()
  {
    $this->title = 'forgot password';
    $this->content = $this->view('Auth.forgot-password');
    $this->layout('admin');
  }
  public function register()
  {
    $model = $this->model('AuthModel');
    if ($model->register($_POST)) {
      $_SESSION['success'] = 'register success';
      header('location:?c=authentication&a=index');
      exit;
    } else {
      $_SESSION['error'] = 'register false';
      header('location:?c=authentication&a=index');
      exit;
    }
  }
  public function store()
  {
    $model = $this->model('AuthModel');
    $result = $model->checkUser($_POST);
    if ($result) {
      if (password_verify($_POST['password'], $result['user_password'])) {
        $_SESSION['success'] = 'Login success';
        $_SESSION['login'] = 'true';
        header('location:?c=student');
        exit;
      } else {
        $_SESSION['error'] = 'Wrong password';
        header('location:?c=authentication');
        exit;
      }
    } else {
      $_SESSION['error'] = 'Account does not exist';
      header('location:?c=authentication');
      exit;
    }
  }
  public function APICheck()
  {
    $model = $this->model('AuthModel');
    $model->checkUser($_GET);
  }
  public function destroy(Request $request)
  {
  }
  public function sendEmail()
  {
    $model = $this->model('AuthModel');
    $result = $model->checkUser($_POST);
    if ($result) {
      $content = $this->view('Auth.content-email', compact('result'));
      $args = [
        'email' => $result['user_email'],
        'username' => $result['user_name'],
        'content' => $content,
      ];
      if (Helper::sendEmail($args)) {
        $_SESSION['success'] = 'Send email success fully';
        header('location:?c=authentication');
        exit;
      } else {
        $_SESSION['error'] = 'send email fail';
        header('location:?c=authentication');
        exit;
      }
    } else {
      $_SESSION['error'] = 'Account does not exist';
      header('location:?c=authentication');
      exit;
    }
  }
  public function changePassword()
  {
    $this->title = 'Change password';
    $this->content = $this->view('Auth.change-password');
    $this->layout('admin');
  }
  public function changePasswordSubmit()
  {
    $model = $this->model('AuthModel');
    if ($model->updateUser($_POST, $_GET['id'])) {
      $_SESSION['success'] = 'Change password successfully';
      header('location:?c=authentication');
      exit;
    } else {
      $_SESSION['error'] = 'Change password fail';
      header('location:?c=authentication');
      exit;
    }
  }
}
